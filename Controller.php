<?php

class Controller {

    const BASE_URL = 'https://letoshop.herokuapp.com/';

    public function __construct(&$session)
    {
        $this->session = &$session;
    }

    public function showCart()
    {
        //adding products to the view
        $cart_products = $this->session['cart'] ?? [];

        //adding total price to the view
        $total_price = $this->session['cart_data']['total_price'] ?? 0;
        require_once('layout/views/cart.php');
    }

    public function showProducts()
    {
        $products = new Products;
        $products = $products->getAllProducts();
        require_once('layout/views/products.php');
    }

    public function addToCart($id)
    {
        $cart = new ShoppingCart($this->session);
        $cart->addToCart($id);
        return header("Location: ".self::BASE_URL.'?action=cart');
    }

    public function remove($id)
    {
        $cart = new ShoppingCart($this->session);
        $cart->removeCart($id);
        return header("Location: ".self::BASE_URL.'?action=cart');
    }

    // public function clear()
    // {
    //     $products = new ShoppingCart($this->session);
    //     $products->deleteCart();
    //     return header("Location: ".self::BASE_URL.'?action=cart');
    // }

    public function checkout()
    {
        $cart_products = $this->session['cart'] ?? [];
        $total_price = $this->session['cart_data']['total_price'] ?? 0;
        $total_amount = $this->session['cart_data']['total_amount'] ?? 0;
        
        require_once('layout/views/checkout.php');
    }

    public function updateQuantities($post)
    {
        $cart = new ShoppingCart($this->session);
        $cart->updateQuantities($post);
        return header("Location: ".self::BASE_URL.'?action=cart');
    }

    public function login()
    {
        $login = new FakeLogin($this->session);
        $login->login();
        return header("Location: ".self::BASE_URL);
    }

    public function logout()
    {
        $login = new FakeLogin($this->session);
        $login->logout();
        return header("Location: ".self::BASE_URL);
    }

    public function createPayment()
    {
        if(!isset($this->session['logged_in']))
        return header("Location: ".self::BASE_URL);

        if( !isset($this->session['cart']) ||  empty($this->session['cart']))
        return header("Location: ".self::BASE_URL);

        require __DIR__  . '/vendor/autoload.php';
        $payer = new PayPal\Api\Payer();
        $payer->setPaymentMethod("paypal");

        foreach($this->session['cart'] as $product)
        {
            $items[] = $item = new PayPal\Api\Item();
            $item->setName($product['name'])
                ->setCurrency('EUR')
                ->setQuantity($product['amount'])
                ->setPrice($product['price']);
        }

        $itemList = new PayPal\Api\ItemList();
        $itemList->setItems($items);

        $details = new PayPal\Api\Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($this->session['cart_data']['total_price']);

        $amount = new PayPal\Api\Amount();
        $amount->setCurrency("EUR")
            ->setTotal($this->session['cart_data']['total_price'])
            ->setDetails($details);

        $transaction = new PayPal\Api\Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl(self::BASE_URL."?action=execute-payment")
            ->setCancelUrl(self::BASE_URL."?action=cart");

        $payment = new PayPal\Api\Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));


        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AbvoANBBBQUAVyiX-vc9ykLh1HTnyj8NRAsS0Wi2z5sFUHOiCNF1ZJDflQ3lAiq3b4p16Y-hxZ95GBhi',     // ClientID
                'EMx91kyYtyK40FQIzLvoWFrnS-77-BI7Yc1ZSFP1oDu973Kk3gdLqKLlMntMIrDIrU58_S8l5HdgomFr'      // ClientSecret
            )
        );

        $payment->create($apiContext);
        $approvalUrl = $payment->getApprovalLink();
        // print_r($approvalUrl);
        return header("Location: ".$approvalUrl);

    }

    public function executePayment()
    {
        if(!isset($this->session['logged_in']))
        return header("Location: ".self::BASE_URL);

        require __DIR__  . '/vendor/autoload.php';
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AbvoANBBBQUAVyiX-vc9ykLh1HTnyj8NRAsS0Wi2z5sFUHOiCNF1ZJDflQ3lAiq3b4p16Y-hxZ95GBhi',     // ClientID
                'EMx91kyYtyK40FQIzLvoWFrnS-77-BI7Yc1ZSFP1oDu973Kk3gdLqKLlMntMIrDIrU58_S8l5HdgomFr'      // ClientSecret
            )
        );

        $paymentId = $_GET['paymentId'];
        $payment = PayPal\Api\Payment::get($paymentId, $apiContext);

        $execution = new PayPal\Api\PaymentExecution();
        $execution->setPayerId($_GET['PayerID']);

        $transaction = new PayPal\Api\Transaction();
        $amount = new PayPal\Api\Amount();
        $details = new PayPal\Api\Details();

        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($this->session['cart_data']['total_price']);

        $amount->setCurrency('EUR');
        $amount->setTotal($this->session['cart_data']['total_price']);
        $amount->setDetails($details);

        $transaction->setAmount($amount);

        $execution->addTransaction($transaction);

        $result = $payment->execute($execution, $apiContext);
        $result = $result->toJSON();

        $result = json_decode($result);

        if($result->state === 'approved')
        {
            // echo '<pre>';
            // print_r($result);
            // echo '</pre>';

            // echo $result->payer->payer_info->email; echo '<br>';
            // echo $result->payer->payer_info->first_name; echo '<br>';
            // echo $result->payer->payer_info->last_name; echo '<br>';
            // echo $result->payer->payer_info->shipping_address->city; echo '<br>';
            // echo $result->transactions[0]->amount->total; echo '<br>';
            // echo '<pre>';
            // print_r($result->transactions[0]->item_list->items);
            // echo '</pre>';

            // ... here save order in the database

            $cart = new ShoppingCart($this->session);
            $cart->deleteCart();
        }

        return header("Location: ".self::BASE_URL);
    }

}
