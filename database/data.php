<?php
// A simple database simulation;
$database = [

    'products' => [
        1=>[
            'id'=>1,
            'name'=>'Sony ',
            'subname'=>'Xperia Z',
            'description'=>'A touchscreen enabled Android smartphone designed, developed and marketed by Sony.',
            'price'=>999.99,
            'url72'=>'assets/img/Sony-Xperia-Z-small.png',
            'url256'=>'assets/img/Sony-Xperia-Z-big.png'
        ],

        2=>[
            'id'=>2,
            'name'=>'iMac ',
            'subname'=>'by Apple',
            'description'=>'The new iMac combines power and performance the ultimate all-in-one desktop experience in two sizes.',
            'price'=>1999.99,
            'url72'=>'assets/img/iMac-icon-small.png',
            'url256'=>'assets/img/iMac-icon-big.png'
        ],

        3=>[
            'id'=>3,
            'name'=>'HP 700 ',
            'subname'=>'Headset',
            'description'=>'EXTENDED BASS: Ultra dynamic range with great sounding highs and lows. Sensitivity: 107dB',
            'price'=>630,
            'url72'=>'assets/img/American-Audio-HP-700-Headset-icon-small.png',
            'url256'=>'assets/img/American-Audio-HP-700-Headset-big-icon.png'
        ],

        4=>[
            'id'=>4,
            'name'=>'Computer ',
            'subname'=>'keyboard',
            'description'=>'Our keyboards labels are a high-quality, durable and economical solution get out one.',
            'price'=>80,
            'url72'=>'assets/img/Keyboard-icon (small).png',
            'url256'=>'assets/img/Keyboard-icon.png'
        ],

        5=>[
            'id'=>5,
            'name'=>'iPad ',
            'subname'=>'by Apple',
            'description'=>'Explore the world of iPad. Featuring iPad Pro in two sizes, buy, and get support.',
            'price'=>899,
            'url72'=>'assets/img/iPad-Front-Apple-small.png',
            'url256'=>'assets/img/iPad-Front-Apple-big.png'
        ],

        6=>[
            'id'=>6,
            'name'=>'M525 Wireless ',
            'subname'=>'Mouse',
            'description'=>'Long 3 Year Battery Life, Ergonomic Shape for Right or Left Hand Use and Micro-Precision Scroll Wheel',
            'price'=>100,
            'url72'=>'assets/img/Mouse-Microsoft-Basic-Optical-v2.0-icon (small).png',
            'url256'=>'assets/img/Mouse-Microsoft-Basic-Optical-v2.0-icon.png'
        ]
    ]

];