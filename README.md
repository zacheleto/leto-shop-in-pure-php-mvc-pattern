# Leto-SHOP-in-pure-PHP-MVC-pattern

A small store that has electronic products, created in pure PHP object oriented MVC-based and have the following features:
* add to shopping cart
* update quqntities
* remove from shopping cart
* calculate the subtotal price of each product and the total price of all added products
* displaying amount of products on cart, on shopping cart and on checkout
* can choose to proceed adding to cart or to checkout
* Fake login system was created
* Fake database was created as a simulation
* you can't checkout untill you login
* PayPal payment integration
* the full process payment can be done through PayPal
* [Buyer PayPal SandBox credentials were provided]
* you can choose to cancel payment and return to store before making payment
* you can see the purchased items on PayPal before making payment
* clearing cart after making payment and returning to the store
* you can see payments history on your paypal account
* business owner can see purchases that were made by client


### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/Leto-SHOP-in-pure-PHP-MVC-pattern.git

## Deployment

https://letoshop.herokuapp.com/

## Built With

* [pure PHP OOP MVC-based]
* [PayPal SandBox]
* [PayPal PHP SDK REST_API open source documentation]

## Author

* **Zache Abdelatif (Leto)**

## License

This project is licensed under the MIT License
