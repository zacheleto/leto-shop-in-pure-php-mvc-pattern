<!-- 
*@Leto Store in pure PHP OOP MVC-based
*@Author: Zache Abdelatif
*@This project is licensed under the MIT License
-->
<?php
// user session for login and update quantities perposes;
session_start();

$session = &$_SESSION;

// includes
include ('include.php');

// including the header
include ('layout/header.php');

// including dynamic view
new Router($_GET, $_POST, $session);

// including the footer
include ('layout/footer.php');
