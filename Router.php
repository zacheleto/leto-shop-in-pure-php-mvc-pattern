<?php

class Router {

    public function __construct($get, $post, &$session)
    {
        $controller = new Controller($session);

        if(isset($get['action']))
        {
            switch($get['action'])
            {
                case 'cart':
                $controller->showCart();
                break;

                case 'add-to-cart':
                $controller->addToCart($get['id']);
                break;

                case 'remove-from-cart':
                $controller->remove($get['id']);
                break;

                // case 'clear-all-from-cart':
                // $controller->clear();
                // break;

                case 'checkout':
                $controller->checkout();
                break;

                case 'update-quantities':
                $controller->updateQuantities($post);
                break;

                case 'login':
                $controller->login();
                break;

                case 'logout':
                $controller->logout();
                break;

                case 'create-payment':
                $controller->createPayment();
                break;

                case 'execute-payment':
                $controller->executePayment();
                break;

                default:
                $controller->showProducts();
            }
        }
        else
        {
            $controller->showProducts();
        }
    }
}
