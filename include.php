<?php
// including the Router class
include ('Router.php');

// including the Controller class
include ('Controller.php');

// including the Products class
include ('Products.php');

// including the FakeLogin class
include ('FakeLogin.php');

// including the ShoppingCart class
include ('ShoppingCart.php');
