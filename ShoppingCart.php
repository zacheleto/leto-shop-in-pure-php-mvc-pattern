<?php

class ShoppingCart {

    private $session;

    function __construct(&$session)
    {
        $this->session = &$session;

        // if no product amounts in the stored in session we set it to 0
        if(!isset($this->session['cart_data']['total_amount']))
        $this->session['cart_data']['total_amount'] = 0;

        // if no product price was stored in session we set it to 0
        if(!isset($this->session['cart_data']['total_price']))
        $this->session['cart_data']['total_price'] = 0.00;
    }

    public function addToCart($id)
    {
        // init Products class to fetch products by ID from the DB
        $products = new Products;
        // If the product ID doesn't exist in session we assign the following
        if(!isset($this->session['cart'][$id]))
        {
            $product = $products->getProductById($id);
            $this->session['cart'][$id]['name'] = $product['name'];
            $this->session['cart'][$id]['subname'] = $product['subname'];
            $this->session['cart'][$id]['url72'] = $product['url72'];
            $this->session['cart'][$id]['price'] = (float) $product['price'];
            $this->session['cart'][$id]['amount'] = 1;
            //The total price here is equal to price because it's added for the first time
            $this->session['cart'][$id]['total_price'] = $this->session['cart'][$id]['price'];

            //Updating session data after adding to cart
            $this->updateCart();
        }
    }

    //Update to cart method
    private function updateCart()
    {
        // Initialize helper vars
        $total_price = 0;
        $total_amount = 0;

        // Looping through all products in shopping cart
        foreach($this->session['cart'] as $index => $value)
        {
            // incrementing total price by the total price curently looped products
            $total_price += $this->session['cart'][$index]['total_price'];
            //Total amount of products added to shopping cart added by the user
            $total_amount += $this->session['cart'][$index]['amount'];
            
        }
        
        $this->session['cart_data']['total_price'] = (float) sprintf('%.2f', $total_price);
        $this->session['cart_data']['total_amount'] = (int) $total_amount;
    }

    //Updating quantities via $post method
    public function updateQuantities($post)
    {
        foreach ($post['amounts'] as $product => $amount) {
            $amount = (int) $amount;
            if($amount < 0 ) $amount = 0;

            $this->session['cart'][$product]['amount'] = $amount;
            $this->session['cart'][$product]['total_price'] = $amount * 
            $this->session['cart'][$product]['price'];
        }
        $this->updateCart();
    }

    //remove product from cart
    public function removeCart($id)
    {
        $this->session['cart'][$id] = null;
        unset($this->session['cart'][$id]);
        $this->updateCart();
    }

    //remove all products from cart
    public function deleteCart()
    {
        $this->session['cart'] = null;
        $this->session['cart_data'] = null;
        unset($this->session['cart']);
        unset($this->session['cart_data']);


    }
    




}
