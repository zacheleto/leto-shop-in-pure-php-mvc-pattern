<!-- Displaying only 3 products per row -->
<?php foreach(array_chunk($products, 3) as $chuncked_products) :?>
<div class="card-deck">

    <?php foreach ($chuncked_products as $product): ?>
    <div class="card" style="width: 18rem;">
        <img src="<?=$product['url256'] ?>" class="card-img-top mt-3" alt="...">
        <div class="card-body">
            <h5 class="card-title text-info"><?=$product['name']. ' ' . $product['subname']?><strong class="text-danger"> <?=$product['price'] ?> €</strong></h5>
            <p class="card-text"><?=$product['description'] ?></p>
            <a href="?action=add-to-cart&id=<?=$product['id']?>" class="btn btn-light">Add to cart</a>
        </div>
    </div>
    <?php endforeach; ?>

</div>
<?php endforeach; ?>

