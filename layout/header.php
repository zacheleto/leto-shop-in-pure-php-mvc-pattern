<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <title>LETO SHOP</title>
</head>
<body class="container">
    <div class="mt-5 container">
        <?php if (isset($session['logged_in'])) : ?>
        <span class="float-right">&nbsp;logged in as <span class="text-info"><?=$session['name']; ?></span> <a href="?action=logout">logout</a> </span>
        <?php else:?>
        <span class="float-right">&nbsp;You are not logged in <a href="?action=login">login</a> </span>
        <?php endif;?>
        <span class="float-right"><a href="?action=cart">Your cart (<?=$session['cart_data']['total_amount'] ?? 0;?>)</a> </span>
        <a href="?action=products"><img src="assets/img/logo.png"></a>
        <p class="text-success">&nbsp; The best online store</p>
        <hr>
        <div class="row">
